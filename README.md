# Vending Machine @ onfido

## Overview

|  |  |
| ------ | ------ |
| Core | Vending Machine library |
| Tests | Unit tests for the vending machine library |
| Console | Demo console app to play with the vending machine  |
|  |  |

### Dependencies

Requires .NET Core 2.0 ([GitHub](https://github.com/dotnet/core))

```sh
$ dotnet --version
```

## Tests

Run unit tests for the vending machine.
```sh
$ cd Tests
$ dotnet test
```

## Demo console application

Run the demo console app to interact with the vending machine.
```sh
$ cd Console
$ dotnet run
```

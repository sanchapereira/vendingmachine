using System;
using System.Collections.Generic;
using System.Linq;

namespace VendingMachineCore
{
    public static class Execute
    {
        public static Product Buy (this VendingMachine vendingMachine, string productDescription, decimal amountInserted, 
            out List<Change> change) 
        {
            var desiredProduct = vendingMachine.Products.Where(x => x.Description == productDescription).FirstOrDefault();
            change = null;
            
            if(desiredProduct == null || (desiredProduct != null && desiredProduct.Quantity == 0))
            {
                return null;
            }

            decimal changeAmount = amountInserted - desiredProduct.Price;
            
            if(amountInserted < desiredProduct.Price)
            {
                throw new Exceptions.InsufficientFundsException(amountInserted, changeAmount);
            }
            
            change = CalculateChange(vendingMachine.GetAvailableChanges(), changeAmount);
            
            //update
            foreach(var c in change)
            {
                vendingMachine.ChangeList.Where(x => x.Value == c.Value).First().Quantity -= c.Quantity;
            }
            
            desiredProduct.Quantity--;

            return new Product(desiredProduct.Description, desiredProduct.Price, 1);
        }

        private static List<Change> CalculateChange(List<Change> availableChange, decimal amount) {
            var ret = new List<Change>();
            int quantity;
            decimal remainingAmount = amount;

            availableChange = availableChange.OrderByDescending(x => x.Value).ToList();
            
            foreach(var c in availableChange)
            {
                if(c.Value > remainingAmount)
                    continue;

                quantity = Math.Min((int)Math.Floor(remainingAmount / c.Value), c.Quantity);

                ret.Add(new Change(c.Value, c.Denomination, quantity));

                remainingAmount -= c.Value * quantity;
            }

            if(remainingAmount == 0)
                return ret;

            throw new Exceptions.NoChangeAvailableException();
        }
        
    }
}
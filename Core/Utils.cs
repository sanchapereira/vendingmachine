using System.IO;

namespace VendingMachineCore
{
    internal static class Utils
    {
        internal static string ReadJsonFile(string filepath)
        {
            if(File.Exists(filepath))
            {
                using(StreamReader fr = new StreamReader(filepath)) 
                {
                    return fr.ReadToEnd();
                }
            } else
            {
                return null;
            }
        }
    }
}
using System.Collections.Generic;
using System.Linq;

namespace VendingMachineCore
{
    public static class Query
    {
        public static List<Product> GetAllProducts(this VendingMachine vendingMachine) => vendingMachine.Products;
        
        public static List<Product> GetAvailableProducts(this VendingMachine vendingMachine)
        {
            return vendingMachine.Products.Where(x => x.Quantity > 0).ToList();
        }

        public static Product GetProductByName(this VendingMachine vendingMachine, string name)
        {
            return vendingMachine.Products.Where(x => x.Description == name).FirstOrDefault();
        }

        public static List<Change> GetAllChanges(this VendingMachine vendingMachine) => vendingMachine.ChangeList;
        
        public static List<Change> GetAvailableChanges(this VendingMachine vendingMachine)
        {
            return vendingMachine.ChangeList.Where(x => x.Quantity > 0).ToList();
        }

        public static decimal GetTotalChangeAmount(this VendingMachine vendingMachine)
        {
            return vendingMachine.ChangeList.Select(x => x.Quantity * x.Value).Sum();
        }
    }
}
using System;

namespace VendingMachineCore.Exceptions
{
    public class InsufficientFundsException : Exception
    {
        public InsufficientFundsException(decimal amountInserted, decimal amountLacking) 
            : base($"Inserted £{amountInserted}, lacks £{amountLacking}") { }
    }
}
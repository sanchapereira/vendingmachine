using System;

namespace VendingMachineCore.Exceptions
{
    public class NoChangeAvailableException : Exception 
    {
        public NoChangeAvailableException() : base("No change available") { }
    }
}
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Linq;

namespace VendingMachineCore
{
    public static class Setup
    {
        public static VendingMachine Initiate(string productsJson = null, string coinsJson = null)
        {
            var vendingMachine = new VendingMachine();

            if(!string.IsNullOrEmpty(productsJson))
            {
                vendingMachine.Products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
            }

            vendingMachine.ChangeList = LoadDefaultCoins();

            if(!string.IsNullOrEmpty(coinsJson))
            {
                var addCoins = JsonConvert.DeserializeObject<List<Change>>(coinsJson);
                foreach(Change c in addCoins)
                    vendingMachine.LoadChange(c.Denomination, c.Quantity);
            }

            return vendingMachine;
        }

        public static VendingMachine InitiateFromJsonFile(string availableProductsJsonPath, string availableCoinsJsonPath)
        {
            string productsJson = Utils.ReadJsonFile(availableProductsJsonPath);
            string changesJson = Utils.ReadJsonFile(availableCoinsJsonPath);

            return Initiate(productsJson, changesJson);
        }

        public static void LoadProduct(this VendingMachine vendingMachine, Product product)
        {
            if(product != null)
            {
                var existing = vendingMachine.Products.Where(x => x.Description == product.Description).FirstOrDefault();
                if(existing != null)
                {
                    existing.Price = product.Price;
                    existing.Quantity += product.Quantity;
                } else
                {
                    vendingMachine.Products.Add(product);
                }
            }   
        }

        private static List<Change> LoadDefaultCoins() 
        {
            return new List<Change>() 
            {
                new Change(0.01m, "1p"),
                new Change(0.02m, "2p"),
                new Change(0.05m, "5p"),
                new Change(0.1m, "10p"),
                new Change(0.2m, "20p"),
                new Change(0.5m, "50p"),
                new Change(1m, "£1"),
                new Change(2m, "£2"),
            };
        }

        public static void LoadChange(this VendingMachine vendingMachine, string coinDenomination, int quantity)
        {
            var existing = vendingMachine.ChangeList.Where(x => x.Denomination == coinDenomination).FirstOrDefault();
            if(existing != null) 
            {
                existing.Quantity += quantity;
            } 
        }
    }
}
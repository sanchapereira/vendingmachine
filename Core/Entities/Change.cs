﻿using System;

namespace VendingMachineCore
{
    public class Change
    {
        public decimal Value { get; internal set; }
        public string Denomination { get; internal set; }
        public int Quantity { get; internal set; }

        public Change(decimal value, string denomination, int quantity = 0) 
        {
            Value = value;
            Denomination = denomination;
            Quantity = quantity;
        }
    }
}

using System;

namespace VendingMachineCore
{
    public class Product
    {
        //public int Id { get; internal set; }
        public string Description { get; internal set; }
        public decimal Price { get; internal set; }
        public int Quantity { get; internal set; }

        public Product (string description, decimal price, int quantity = 0) 
        {
            Description = description;
            Price = price;
            Quantity = quantity;
        }
    }
}

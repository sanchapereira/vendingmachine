using System.Collections.Generic;

namespace VendingMachineCore
{
    public class VendingMachine
    {
        internal List<Product> Products { get; set; }
        internal List<Change> ChangeList { get; set; }

        internal VendingMachine()
        {
            Products = new List<Product>();
            ChangeList = new List<Change>();
        }
    }   
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using VendingMachineCore;

namespace VendingMachineConsole
{
    class Program
    {
        private static VendingMachineCore.VendingMachine _vendingMachine;

        static void Main(string[] args)
        {
            Console.WriteLine("=== Awesome vending machine @ onfido ===");
            PrintMainScreen();

            _vendingMachine = Setup.InitiateFromJsonFile("products.json", "changes.json");

            while(true)
            {
                string command = Console.ReadLine();

                switch(command)
                {
                    case "1":
                        ActAsClient();
                        break;
                    case "2":
                        ActAsManager();
                        break;
                    case "exit":
                        return;
                    default:
                        PrintMainScreen();
                        break;
                }
            }
        }

        private static void ActAsClient()
        {
            PrintClientScreen();
            while(true)
            {
                string command = Console.ReadLine();

                switch(command)
                {
                    case "1": //see products
                        _vendingMachine.PrintAvailableProducts();
                        break;
                    case "2": //buy product
                        _vendingMachine.PrintBuyProduct();
                        break;
                    case "9": //go back
                        PrintMainScreen();
                        return;
                }
            }
        }

        private static void ActAsManager()
        {
            PrintManagerScreen();
            while(true)
            {
                string command = Console.ReadLine();

                switch(command)
                {
                    case "1": //see products
                        _vendingMachine.PrintAllProducts();
                        break;
                    case "2": //see changes
                        _vendingMachine.PrintAllChange();
                        break;
                    case "3": //reload product
                        _vendingMachine.PrintLoadProduct();
                        break;
                    case "4": //reload change
                        _vendingMachine.PrintLoadChange();
                        break;
                    case "9": //go back
                        PrintMainScreen();
                        return;
                }
            }
        }

        private static void PrintMainScreen()
        {
            Console.WriteLine("Press <1> to act as client");
            Console.WriteLine("Press <2> to manage vending maching");
            Console.WriteLine("Or leave by pressing <exit>");
        }

        private static void PrintClientScreen()
        {
            Console.WriteLine("Press <1> to see products");
            Console.WriteLine("Press <2> to buy a product");
            Console.WriteLine("Press <9> to go back");
        }

        private static void PrintManagerScreen()
        {
            Console.WriteLine("Press <1> to see all products");
            Console.WriteLine("Press <2> to see changes");
            Console.WriteLine("Press <3> to reload product");
            Console.WriteLine("Press <4> to reload change");
            Console.WriteLine("Press <9> to go back");
        }
    }
}

using System;
using System.Linq;
using VendingMachineCore;
using VendingMachineCore.Exceptions;

namespace VendingMachineConsole
{
    public static class CoreExtensions
    {
        public static void PrintBuyProduct(this VendingMachine vendingMachine)
        {
            Console.Write("select product: ");
            string pName = Console.ReadLine();

            Console.Write("insert money: ");
            string insertedAmount = Console.ReadLine();

            try 
            {
                var product = vendingMachine.Buy(pName, decimal.Parse(insertedAmount), out var change);

                if(product != null) {
                    Console.WriteLine($"Bought {product.Description}");
                    if(change != null) {
                        decimal totalChange = change.Select(x => x.Value * x.Quantity).Sum();
                       
                        string changeDetail = "";
                        foreach(var c in change)
                        {
                            for(int i = 0; i < c.Quantity; i++)
                                changeDetail += $"{c.Denomination} ";
                        }

                         Console.WriteLine($"Received £{totalChange} in change: {changeDetail}");
                    }
                }
                else
                {
                    Console.WriteLine("unavailable");
                }
            } 
            catch (Exception ex)
            {
                if (ex is InsufficientFundsException || ex is NoChangeAvailableException)
                {
                    Console.WriteLine(ex.Message);
                }
                else
                {
                    Console.WriteLine("unexpected error!");
                }
            }
        }

        public static void PrintAvailableProducts(this VendingMachine vendingMachine)
        {
            var products = vendingMachine.GetAvailableProducts();
            if(products.Count > 0)
            {
                Console.WriteLine("| product                 | price (£) |");
                foreach(var p in products)
                {
                    Console.WriteLine($"| {p.Description, -23} | {p.Price, 9} |");
                }
            } else
            {
                Console.WriteLine("vending machine is empty");
            }
        }

        public static void PrintAllProducts(this VendingMachine vendingMachine)
        {
            var products = vendingMachine.GetAllProducts();
            if(products.Count > 0)
            {
                Console.WriteLine("| product                 | price (£) | quantity |");
                foreach(var p in products)
                {
                    Console.WriteLine($"| {p.Description, -23} | {p.Price, 9} | {p.Quantity, 8} |");
                }
            } else
            {
                Console.WriteLine("vending machine is empty");
            }
        }

        public static void PrintAllChange(this VendingMachine vendingMachine)
        {
            var changes = vendingMachine.GetAllChanges();
            Console.WriteLine("| coin   | quantity |");
            foreach(var c in changes)
            {
                Console.WriteLine($"| {c.Denomination, -6} | {c.Quantity, 8} |");
            }
            Console.WriteLine($"Total change is £{vendingMachine.GetTotalChangeAmount()}");
        }

        public static void PrintLoadProduct(this VendingMachine vendingMachine)
        {
            Console.Write("select product: ");
            string p_description = Console.ReadLine();
            
            Console.Write("price: ");
            string p_price = Console.ReadLine();
            
            Console.Write("quantity to add: ");
            string p_quantity = Console.ReadLine();
                        
            vendingMachine.LoadProduct(new Product(p_description, decimal.Parse(p_price), int.Parse(p_quantity)));
            Console.WriteLine("done!");
        }

        public static void PrintLoadChange(this VendingMachine vendingMachine)
        {
            Console.Write("select coin: ");
            string c_denomination = Console.ReadLine();

            Console.Write("quantity to add: ");
            string c_quantity = Console.ReadLine();

            vendingMachine.LoadChange(c_denomination, int.Parse(c_quantity));
            Console.WriteLine("done!");
        }
    }
}
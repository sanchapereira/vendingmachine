using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using VendingMachineCore;
using VendingMachineCore.Exceptions;

namespace VendingMachineTests
{
    [TestClass]
    public class ExecuteTests
    {
        [TestMethod]
        [ExpectedException(typeof(NoChangeAvailableException))]
        public void BuyProduct_WhenNoChangeAvailable_ShouldThrowNoChangeAvailable()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadProduct(new Product("Coca-Cola", 2.50m, 10));

            vendingMachine.LoadChange("5p", 10);
            vendingMachine.LoadChange("10p", 10);

            vendingMachine.Buy("Coca-Cola", 5, out var change);
        }

        [TestMethod]
        public void BuyProduct_WhenNoChangeAvailableButInsertedPrice_IsSuccessful()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadProduct(new Product("Coca-Cola", 2.50m, 10));

            var p = vendingMachine.Buy("Coca-Cola", 2.5m, out var change);

            Assert.AreEqual(p.Description, "Coca-Cola", "Should be able to buy product without change if amount inserted is equal to price");
        }

        [TestMethod]
        public void BuyProduct_WhenProductNotAvailable()
        {
            var vendingMachine = Setup.Initiate();
            //vendingMachine.LoadProduct(new Product("Sandwish", 4.80m));

            var p = vendingMachine.Buy("Sandwish", 4.8m, out var change);

            Assert.AreEqual(p, null, "No sandwish is available so it should not be returned");
        }

        [TestMethod]
        [ExpectedException(typeof(InsufficientFundsException))]
        public void BuyProduct_WhenInsertedLessThanPrice_ShouldThrowInsufficientFunds()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadProduct(new Product("Water", 1.50m, 10));

            vendingMachine.Buy("Water", 1m, out var change);
        }

        [TestMethod]
        public void BuyProduct_ChangeIsCorrect()
        {
            var vendingMachine = Setup.Initiate();

            vendingMachine.LoadChange("5p", 10);
            vendingMachine.LoadChange("10p", 10);
            vendingMachine.LoadChange("20p", 10);
            vendingMachine.LoadChange("50p", 10);
            vendingMachine.LoadChange("£1", 10);
            vendingMachine.LoadChange("£2", 10);
            
            vendingMachine.LoadProduct(new Product("Water", 1.50m, 1));
            
            decimal expectedChange = 2.7m;

            var p = vendingMachine.Buy("Water", 4.2m, out var change);
            decimal actualChange = change.Select(x => x.Value * x.Quantity).Sum();

            Assert.AreEqual(expectedChange, actualChange, $"Change should be {expectedChange} but instead was {actualChange}");
        }

        [TestMethod]
        public void QuantityOfProductDecreases()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadProduct(new Product("Water", 1.50m, 2));

            var p = vendingMachine.Buy("Water", 1.5m, out var change);

            int actualQuantity = vendingMachine.GetAllProducts().Where(x => x.Description == "Water").First().Quantity;
            Assert.IsTrue(actualQuantity == 1, "Water quantity should have decreased");
        }
    }
}
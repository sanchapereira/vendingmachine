using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachineCore;

namespace VendingMachineTests
{
    [TestClass]
    public class SetupTests
    {
        [TestMethod]
        public void VendingMachine_IsOperational()
        {
            var vendingMachine = Setup.Initiate();

            bool assert = vendingMachine != null && vendingMachine.GetAllProducts() != null && vendingMachine.GetAllChanges() != null;
            Assert.IsTrue(assert, "Vending machine should be fully instanciated");
        }

        [TestMethod]
        public void VendingMachine_NoAvailableProducts()
        {
            var vendingMachine = Setup.Initiate();

            Assert.IsFalse(vendingMachine.GetAvailableProducts().Count > 0, "vending machine should not have any available products");
        }

        [TestMethod]
        public void VendingMachine_TwoDifferentTypesOfProducts()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadProduct(new Product("Coca-Cola", 2.0m));
            vendingMachine.LoadProduct(new Product("Guiness Beer", 4.0m));

            Assert.IsTrue(vendingMachine.GetAllProducts().Count == 2, "vending machine should have exactly 2 products");
        }

        [TestMethod]
        public void VendingMachine_Only1AvailableTypeOfProduct()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadProduct(new Product("Coca-Cola", 2.0m, 5));
            vendingMachine.LoadProduct(new Product("Guiness Beer", 4.0m));

            Assert.IsTrue(vendingMachine.GetAvailableProducts().Count == 1, "vending machine should have only 1 available product");
        }

        [TestMethod]
        public void VendingMachine_NoChangesAvailable()
        {
            var vendingMachine = Setup.Initiate();

            Assert.IsFalse(vendingMachine.GetTotalChangeAmount() > 0, "vending machine should not have any change available");
        }

        [TestMethod]
        public void VendingMachine_HasChanges()
        {
            var vendingMachine = Setup.Initiate();
            vendingMachine.LoadChange("1p", 5);
            vendingMachine.LoadChange("£2", 1);
            vendingMachine.LoadChange("1p", -1);

            Assert.IsTrue(vendingMachine.GetAllChanges().Count == 8 && vendingMachine.GetTotalChangeAmount() == 2.04m, 
                $"vending machine should have a total of £2.05 but instead has £{vendingMachine.GetTotalChangeAmount()}");
        }
    }
}
